
# Firefox plugin and native client
This project contains the  [Firefox plugin](https://gitlab.com/arpa2/http_sasl_client/-/tree/master/plugin)  and the [Native client](https://gitlab.com/arpa2/http_sasl_client/-/tree/master/native) enabling [HTTP-SASL](http://internetwide.org/blog/2018/11/15/somethings-cooking-4.html)

<img src="https://mansoft.nl/arpa2/http-sasl-client.svg" width="100%" />
