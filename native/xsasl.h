extern "C" {
#include <arpa2/xover-sasl.h>
}

class Xsasl
{
private:
    XoverSASL m_sasl;   
    membuf bufToMemBuf(const void *buf, int length);
    membuf strToMemBuf(const char *str);
public:
    Xsasl();
    ~Xsasl();

    static void init();
    static void fini();

    bool open(const char *service);
    bool set_server_realm(const char *realm);
    bool set_chanbind(bool enforce, const void *buf, int length);
    bool set_username(const char *username);
    bool set_alias(const char *alias);
    bool set_realm(const char *realm);
    void set_password(const char *password);
    bool set_mech(const char *mech);
    bool step_client(const void *s2c, size_t s2c_len, const char **mech, size_t *mech_len, const void **c2s, size_t *c2s_len);
    bool get_outcome(bool *success, bool *failure);
};
