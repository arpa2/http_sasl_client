//import related modules
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import nl.mansoft.backend 1.0

//window containing the application
ApplicationWindow {
    visible: backend.visible
    //title of the application
    title: qsTr("SASL login")
    width: 640
    height: 480

    BackEnd {
        id: backend
    }
    //Content Area

    GridLayout {
        id: grid
        columns: 2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text { text: "URL"}
        Text { text: backend.url }
        Text { text: "Realm"}
        Text { text: backend.realm }
        Text { text: "User"}
        TextField { text: backend.userName; onEditingFinished: backend.userName = text }
        Text { text: "Alias"}
        TextField { text: backend.alias; onEditingFinished: backend.alias = text }
        Text { text: "Password"; }
        TextField { text: backend.password; echoMode: TextField.Password; onEditingFinished: backend.password = text }
        Text { text: "Your domain"; }
        TextField { text: backend.domain; onEditingFinished: backend.domain = text }
        Text { text: "Mechanisms"; }
        ComboBox {
            model: backend.mechs
            onCurrentIndexChanged: backend.mech = backend.mechs[currentIndex]
        }

        Button {
            text: qsTr("Login")
            onClicked: backend.login()
            enabled: backend.loginEnabled
        }

        Button {
            text: qsTr("Cancel")
            onClicked: backend.cancel()
            enabled: backend.loginEnabled
        }
    }

}
