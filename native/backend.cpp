#include <QtCore/qcoreapplication.h>
#include <QDebug>
#include <QUrl>
#include <QByteArray>

#include <iostream>

#include "backend.h"
extern "C" {

#include <arpa2/kip.h>
}

#define HTTP_SERVICE_NAME "HTTP"

static const char service[] = HTTP_SERVICE_NAME;

BackEnd::BackEnd(QObject *parent) :
    QObject(parent)
{
    m_sasl = NULL;
    Xsasl::init();

    // listen to console input
    QObject::connect(
        &m_console,
        &QConsoleListener::newLine,
        [this](const QString &strNewLine) {
            if (strNewLine.isEmpty()) {
                qDebug() << "EOF";
                QCoreApplication::exit(-2);
            } else {
                qDebug() << "JSON:" << strNewLine;
                QByteArray ba = strNewLine.toUtf8();
                QJsonParseError error;
                QJsonDocument docIn = QJsonDocument::fromJson(ba, &error);
                QJsonObject input = docIn.object();
                m_httpSaslOut = QJsonObject();
                m_httpSaslOut["s2s"] = input["s2s"];
                m_httpSaslOut["requestId"] = input["requestId"];
                QJsonValue json_extraInfoSpec = input["extraInfoSpec"];
                m_httpSaslOut["extraInfoSpec"] = json_extraInfoSpec;
                
                qDebug() << error.errorString();

                QJsonValue json_mechs = input["mech"];
                if (json_mechs.isString()) {
                    QString mechs = json_mechs.toString();
                    QStringList mechsList = mechs.split(' ');
                    setMechs(mechsList);
                    qDebug() <<  "mechs:" << mechs;
                    const char *service = getenv("HTTP_SERVICE_NAME");
                    if (!service) {
                        service = HTTP_SERVICE_NAME;
                    }
                    qDebug() << "HTTP service name:" << service;

                    closeSasl();
                    m_sasl = new Xsasl();
                    assert(m_sasl->open(service));
                    qDebug() << "Created new sasl:" << m_sasl;
                    if (json_extraInfoSpec.isObject()) {
                        QJsonValue json_redirectUrl = json_extraInfoSpec.toObject()["redirectUrl"];
                        if (json_redirectUrl.isString()) {
                            setUrl(json_redirectUrl.toString());
                            QUrl qURL = QUrl(m_url);
                            const QString host = qURL.host();
                            qDebug() <<  "host:" << host;
                            assert(m_sasl->set_server_realm(qUtf8Printable(host)));
                        }
                    }
                    char *str_login = getenv("KIPSERVICE_CLIENTUSER_LOGIN");
                    if (str_login) {
                        qDebug() << "login:" << str_login;
                        setUserName(QString(str_login));
                    }
                    char *str_password = getenv("QUICKSASL_PASSPHRASE");
                    if (str_password) {
                        qDebug() << "password:" << str_password;
                        setPassword(QString(str_password));
                    }
                    char *str_alias = getenv("KIPSERVICE_CLIENTUSER_ACL");
                    if (str_alias) {
                        qDebug() << "alias:" << str_alias;
                        setAlias(QString(str_alias));
                    }
                    char *str_domain = getenv("KIPSERVICE_CLIENT_REALM");
                    if (str_domain) {
                        qDebug() << "domain:" << str_domain;
                        setDomain(QString(str_domain));
                    }
                    QJsonValue json_realm = input["realm"];
                    if (json_realm.isString()) {
                        setRealm(json_realm.toString());
                    }

                    QJsonValue json_mechs = input["mech"];
                    if (json_mechs.isString()) {
                        QString mechs = json_mechs.toString();
                        QStringList mechsList = mechs.split(' ');
                        setMechs(mechsList);
                        qDebug() <<  "mechs:" << mechs;

                    }
                    QJsonValue json_channelbinding = input["channelBinding"];
                    if (json_channelbinding.isString()) {
                        QString channelBinding = json_channelbinding.toString();
                        qDebug() <<  "channelBinding:" << channelBinding;
                        QByteArray result = QByteArray::fromBase64(QByteArray(qUtf8Printable(channelBinding)));
                        assert(m_sasl->set_chanbind(false, result, result.length()));
                    }
                    char *str_headless = getenv("HEADLESS");
                    if (str_headless) {
                        login();
                    } else {
                        setLoginEnabled(true);
                        setVisible(true);
                    }
                } else {
                    qDebug() << "Using existing sasl:" << m_sasl;
                    QJsonValue json_s2c = input["s2c"];
                    if (json_s2c.isString()) {
                        QString s2c = json_s2c.toString();
                        qDebug() << "s2c:" << s2c;
                        QByteArray result = QByteArray::fromBase64(QByteArray(qUtf8Printable(s2c)));
                        sendResponse(result, result.length());
                    } else {
                        qDebug() << "error decoding:" << json_s2c;
                    }
                }
            }
        }
    );
    qDebug() << "Listening to console input:";
}

BackEnd::~BackEnd()
{
    qDebug() << "BackEnd::~BackEnd()";
    Xsasl::fini();
}

void BackEnd::closeSasl()
{
    delete m_sasl;
    m_sasl = NULL;
}

void BackEnd::sendResponse()
{
    QJsonDocument docOut = QJsonDocument(m_httpSaslOut);
    QByteArray ba_out = docOut.toJson(QJsonDocument::Compact);

    qDebug() << "ba_out:" << ba_out;
    int len = ba_out.length();
    std::cout.put(len);
    std::cout.put(len >> 8);
    std::cout.put(len >> 16);
    std::cout.put(len >> 24);
    std::cout << ba_out.toStdString();
    std::cout.flush();
}

void BackEnd::sendResponse(const void *s2c_buf, int length)
{
    qDebug() << "sendResponse, m_sasl:" << m_sasl;
    qDebug() << "calling xsasl_step_client, s2c:" << s2c_buf << "length:" << length;
    const char *mech;
    size_t mech_len;
    const void *c2s;
    size_t c2s_len;
    bool success = false;
    bool failure = true;
    if (m_sasl->step_client (s2c_buf, length, &mech, &mech_len, &c2s, &c2s_len))
    {
        qDebug() << "xsasl_step_client, c2s:" << c2s << "c2s len:" << c2s_len;
        /* Be sure that the _entered_ password is set for upcoming SASL steps */
        m_sasl->set_password(qUtf8Printable(m_password));
        QByteArray c2s_arr((const char *) c2s, c2s_len);
        if (mech != NULL) {
            QString chosen_mech = QString::fromUtf8((const char *) mech, mech_len);
            qDebug() << "chosen mechanism:" << chosen_mech;
            m_httpSaslOut["mech"] = chosen_mech;
        }
        m_httpSaslOut["realm"] = m_domain;
        m_httpSaslOut["c2s"] = QString::fromUtf8(c2s_arr.toBase64().data());
        qDebug() << "###:" << m_httpSaslOut["c2s"];
        sendResponse();
        m_sasl->get_outcome(&success, &failure);
    } else {
        qDebug() << "xsasl_step_client failed";
    }
    qDebug() << "success:" << success << "failure:" << failure;
    if (success || failure) {
        closeSasl();
    }
}

QString BackEnd::url()
{
    return m_url;
}

void BackEnd::setUrl(const QString &url)
{
    if (url == m_url)
        return;

    m_url = url;
    emit urlChanged();
}

QString BackEnd::realm()
{
    return m_realm;
}

void BackEnd::setRealm(const QString &realm)
{
    if (realm == m_realm)
        return;

    m_realm = realm;
    emit realmChanged();
}

QString BackEnd::userName()
{
    return m_username;
}

void BackEnd::setUserName(const QString &userName)
{
    if (userName == m_username)
        return;

    m_username = userName;
    emit userNameChanged();
}

QString BackEnd::password()
{
    return m_password;
}

void BackEnd::setPassword(const QString &password)
{
    if (password == m_password)
        return;

    m_password = password;
    emit passwordChanged();
}

QString BackEnd::alias()
{
    return m_alias;
}

void BackEnd::setAlias(const QString &alias)
{
    if (alias == m_alias)
        return;

    m_alias = alias;
    emit aliasChanged();
}

QString BackEnd::domain()
{
    return m_domain;
}

void BackEnd::setDomain(const QString &domain)
{
    if (domain == m_domain)
        return;

    m_domain = domain;
    emit domainChanged();
}

bool BackEnd::loginEnabled()
{
    return m_loginEnabled;
}

void BackEnd::setLoginEnabled(const bool enabled)
{
    if (enabled == m_loginEnabled)
        return;
    m_loginEnabled = enabled;
    emit loginEnabledChanged();
}

bool BackEnd::visible()
{
    return m_visible;
}

void BackEnd::setVisible(const bool visible)
{
    if (visible == m_visible)
        return;
    m_visible = visible;
    emit visibleChanged();
}

QStringList BackEnd::mechs()
{
    return m_mechs;
}

void BackEnd::setMechs(QStringList mechs)
{
    if (mechs == m_mechs)
        return;
    m_mechs = mechs;
    emit mechsChanged();
}

QString BackEnd::mech()
{
    return m_mech;
}

void BackEnd::setMech(QString mech)
{
    if (mech == m_mech)
        return;
    m_mech = mech;
    emit mechChanged();
}

void BackEnd::login()
{
    qDebug() << "username:" << m_username << "alias:" << m_alias << "password:" << m_password << "domain:" << m_domain;
    qDebug() << "mech:" << m_mech;
    assert(m_sasl->set_username(qUtf8Printable(m_username)));
    assert(m_sasl->set_alias(qUtf8Printable(m_alias)));
    assert(m_sasl->set_realm(qUtf8Printable(m_domain)));
    /* Specifically SXOVER-PLUS starts with a KIP phase using its own password */
    m_sasl->set_password(qUtf8Printable( (m_mech == "SXOVER-PLUS") ? getenv("KIPSERVICE_PASSPHRASE") : m_password));
    assert(m_sasl->set_mech(qUtf8Printable(m_mech)));
    setVisible(false);
    setLoginEnabled(false);
    sendResponse(NULL, 0);
}

void BackEnd::cancel()
{
    qDebug() << "cancel";
    closeSasl();
    m_httpSaslOut.remove("extraInfoSpec");
    setVisible(false);
    setLoginEnabled(false);
    sendResponse();
}

