#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <qqml.h>

#include "qconsolelistener.h"
#include "xsasl.h"

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString realm READ realm WRITE setRealm NOTIFY realmChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString alias READ alias WRITE setAlias NOTIFY aliasChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString domain READ domain WRITE setDomain NOTIFY domainChanged)
    Q_PROPERTY(bool loginEnabled READ loginEnabled WRITE setLoginEnabled NOTIFY loginEnabledChanged)
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(QStringList mechs READ mechs WRITE setMechs NOTIFY mechsChanged)
    Q_PROPERTY(QString mech READ mech WRITE setMech NOTIFY mechChanged)

public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();
    QString url();
    void setUrl(const QString &url);
    QString realm();
    void setRealm(const QString &url);
    QString userName();
    void setUserName(const QString &username);
    QString password();
    void setPassword(const QString &password);
    QString alias();
    void setAlias(const QString &alias);
    QString domain();
    void setDomain(const QString &domain);
    bool loginEnabled();
    void setLoginEnabled(const bool enabled);
    bool visible();
    void setVisible(const bool visible);
    QStringList mechs();
    void setMechs(QStringList mechs);
    QString mech();
    void setMech(QString mech);
    Q_INVOKABLE void login();
    Q_INVOKABLE void cancel();
signals:
    void urlChanged();
    void realmChanged();
    void userNameChanged();
    void passwordChanged();
    void aliasChanged();
    void domainChanged();
    void loginEnabledChanged();
    void visibleChanged();
    void mechsChanged();
    void mechChanged();

private:
    void closeSasl();
    void sendResponse();
    void sendResponse(const void *s2c, int length);
    QString m_url;
    QString m_realm;
    QString m_username;
    QString m_password;
    QString m_alias;
    QString m_domain;
    bool m_loginEnabled;
    bool m_visible;
    QStringList m_mechs;
    QString m_mech;
    QConsoleListener m_console;
    Xsasl *m_sasl;
    QJsonObject m_httpSaslOut;
};

#endif // BACKEND_H
