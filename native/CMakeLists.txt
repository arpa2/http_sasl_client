set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Core Quick REQUIRED)

add_executable(sasl-login
  main.cpp
  backend.cpp
  xsasl.cpp
  qconsolelistener.cpp
  qml.qrc
)

target_compile_definitions(sasl-login
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(sasl-login
  PRIVATE Qt5::Core Qt5::Quick
  ARPA2::XoverSASL
  ARPA2::Quick-DER
  ARPA2::Kip
)
