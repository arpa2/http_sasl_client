#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include "backend.h"

int main(int argc, char *argv[])
{
    if (getenv("HEADLESS")) {
        QCoreApplication app(argc, argv);
        BackEnd backend;
        return app.exec();
    } else {
        QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

        QGuiApplication app(argc, argv);
        qmlRegisterType<BackEnd>("nl.mansoft.backend", 1, 0, "BackEnd");

        QQmlApplicationEngine engine;
        const QUrl url(QStringLiteral("qrc:/main.qml"));
        QObject::connect(
            &engine,
            &QQmlApplicationEngine::objectCreated,
            &app,
            [url](QObject *obj, const QUrl &objUrl) {
                if (!obj && url == objUrl)
                    QCoreApplication::exit(-1);
            },
            Qt::QueuedConnection
        );
        engine.load(url);
        return app.exec();
    }
}
