#include <iostream>
#include "qconsolelistener.h"
#include <QDebug>

QConsoleListener::QConsoleListener()
{
    QObject::connect(
        this, &QConsoleListener::finishedGetLine,
        this, &QConsoleListener::on_finishedGetLine,
        Qt::QueuedConnection
    );
#ifdef Q_OS_WIN
    m_notifier = new QWinEventNotifier(GetStdHandle(STD_INPUT_HANDLE));
#else
    m_notifier = new QSocketNotifier(fileno(stdin), QSocketNotifier::Read);
#endif
    // NOTE : move to thread because std::getline blocks,
    //        then we sync with main thread using a QueuedConnection with finishedGetLine
    m_notifier->moveToThread(&m_thread);
    QObject::connect(
        &m_thread , &QThread::finished,
        m_notifier, &QObject::deleteLater
    );
#ifdef Q_OS_WIN
    QObject::connect(m_notifier, &QWinEventNotifier::activated,
#else
    QObject::connect(m_notifier, &QSocketNotifier::activated,
#endif
    [this]() {
        qDebug() << "QConsoleListener thread entry";
        bool eof = true;
        int byte1 = std::cin.get();
        if (byte1 != EOF) {
            int byte2 = std::cin.get();
            if (byte2 != EOF) {
                int byte3 = std::cin.get();
                if (byte3 != EOF) {
                    int byte4 = std::cin.get();
                    if (byte4 != EOF) {
                        eof = false;
                        int length = (byte1) | (byte2 << 8) | (byte3 << 16) | (byte4 << 24);
                        qDebug() << "length " << length;
                        char str[length];
                        std::cin.read(str, length);
                        QString strLine = QString::fromUtf8(str, length);
                        Q_EMIT this->finishedGetLine(strLine);
                    }
                }
            }
        }
        if (eof) {
            qDebug() << "QConsoleListener EOF";
            Q_EMIT this->finishedGetLine(QString());
            m_thread.quit();
        }
        qDebug() << "QConsoleListener thread exit";
    });
    m_thread.start();
}

void QConsoleListener::on_finishedGetLine(const QString &strNewLine)
{
    Q_EMIT this->newLine(strNewLine);
}

QConsoleListener::~QConsoleListener()
{
    if (m_thread.isRunning()) {
        m_thread.quit();
    }
    m_thread.wait();
}
