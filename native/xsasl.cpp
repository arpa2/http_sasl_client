#include <QDebug>
#include <QString>

extern "C" {
#include <arpa2/kip.h>
}
#include "xsasl.h"

Xsasl::Xsasl()
{
    m_sasl = NULL;
}

Xsasl::~Xsasl()
{
    qDebug() << "closing sasl:" << m_sasl;
    xsasl_close(&m_sasl);
    unsetenv("QUICKSASL_PASSPHRASE");
}

void Xsasl::init()
{
    kip_init();
    kipservice_init(NULL, NULL);
    xsasl_init(NULL, "InternetWide_Identity");
}

void Xsasl::fini()
{
    xsasl_fini();
    kipservice_fini();
    kip_fini();
}

membuf Xsasl::bufToMemBuf(const void *buf, int length) {
    uint8_t *data = NULL;
    membuf mbuf = {
        .bufptr = NULL,
        .buflen = 0
    };
    if (qsasl_alloc(m_sasl, length, &data)) {
        memcpy(data, buf, length);
        mbuf.bufptr = data;
        mbuf.buflen = length;
    }
    return mbuf;
}

membuf Xsasl::strToMemBuf(const char *str) {
    return bufToMemBuf(str, strlen(str));
}

bool Xsasl::open(const char *service)
{
    return xsasl_client(&m_sasl, service, NULL, NULL, 0);
}

bool Xsasl::set_server_realm(const char *realm)
{
    membuf crs_realm = strToMemBuf(realm);
    return xsasl_set_server_realm(m_sasl, crs_realm);
}

bool Xsasl::set_chanbind(bool enforce, const void *buf, int length)
{
    membuf chanbindbuf = bufToMemBuf(buf, length);
    return xsasl_set_chanbind(m_sasl, enforce, chanbindbuf);
}

bool Xsasl::step_client(const void *s2c, size_t s2c_len, const char **mech, size_t *mech_len, const void **c2s, size_t *c2s_len)
{
    dercursor in_s2c = { (uint8_t *) s2c, s2c_len };
    dercursor out_mech;
    dercursor out_c2s = { (uint8_t*) "", 0 };
    bool rc = xsasl_step_client (m_sasl, in_s2c, &out_mech, &out_c2s);
    if (rc) {
        *mech = (const char *) out_mech.derptr;
        *mech_len = out_mech.derlen;
        *c2s = out_c2s.derptr;
        *c2s_len = out_c2s.derlen;
    }
    return rc;
}

bool Xsasl::set_username(const char *username)
{
    membuf crs_login = strToMemBuf(username);
    return xsasl_set_clientuser_login(m_sasl, crs_login);
}

bool Xsasl::set_alias(const char *alias)
{
    membuf crs_acl = strToMemBuf(alias);
    return xsasl_set_clientuser_acl(m_sasl, crs_acl);
}

bool Xsasl::set_realm(const char *realm)
{
    membuf crs_realm = strToMemBuf(realm);
    bool rc = xsasl_set_client_realm(m_sasl, crs_realm);
    if (rc) {
        setenv("KIP_REALM", realm, 1);
    }
    return rc;
}

void Xsasl::set_password(const char *password)
{
    qDebug() << "Xsasl::set_password, password:" << password;
    setenv("QUICKSASL_PASSPHRASE", password, 1);
}

bool Xsasl::set_mech(const char *mech)
{
    membuf mem_mech = strToMemBuf(mech);
    dercursor csr_mech = {
        .derptr = mem_mech.bufptr,
        .derlen = mem_mech.buflen
    };
    return xsasl_set_mech(m_sasl, csr_mech);
}

bool Xsasl::get_outcome(bool *success, bool *failure)
{
    return xsasl_get_outcome (m_sasl, success, failure);
}

